# README #

### What is this repository for? ###
This Java application is using  multi threading and can be used for checking health of any domain/server. This is a Maven based project.
It checks health of any server through ICMP Ping, TCP and Trace Route on a configurable interval.
If server is running fine then it just adds logs otherwise add error in logs and send error report to an external server through POST API request.

### How do I get set up? ###

* To setup the project in your eclipse follow the below steps.
	* Download the code
	* Import the project in Eclipse using existing Maven project option
	* Do maven clean and install to download all the required jars (maven dependencies)
	* There will be 2 logs files "PingApplication.log" and "PingApplicationReport.log" in root folder of project, parallel to "src"
	* I have added Source folder having "icmp4j" source code if required
	
* Configuration
	* You cam add comma separated multiple hosts to check in "ping.properties"
	* Change the API URL in "ping.properties"  i.e. report.post.api.url 
	* Run "PingApplication.java" as java application

* Dependencies
	* JDK 1.8
	* Maven
	* org.apache.httpcomponents
	* com.googlecode.json-simple
	* org.apache.commons
	* org.slf4j
	* org.icmp4j (Jar placed in ${basedir}/lib/ folder)
	* org.shortpasta.icmp2 (Jar placed in ${basedir}/lib/ folder)
	* org.shortpasta.icmp2.platform (Jar placed in ${basedir}/lib/ folder)

* Deployment instructions
	* You may export as Jar and run with following command in CMD
			java -jar PingApplication.jar
			

### Contribution guidelines ###

* Writing tests
	You may add your code to fix any bug (if any) or to extend the functionality.
	
* Code review
	Code should be up to the mark and there should not be any compilation error.
	Add instructions if required.
	

### Who do I talk to? ###

* You may reach to me at krishan.babbar@gmail.com
* GeeksQuest.com

### Errors and Solutions ###
Error1: In case you get error code 87 e.g. "Response: Error: Last error: 87", then you are not passing all the parameters.
Solution: I set all the following parameters and error is resolved.
	icmpPingRequest.setHost("google.com");
	icmpPingRequest.setPacketSize(1024);
	icmpPingRequest.setTimeout(5000);
	icmpPingRequest.setTtl(100);
	
Error 2: While creating jar, it was creating resources folder also in jar and it was not working because log4j.properties should be on root.
Solution: https://stackoverflow.com/questions/7263505/how-to-package-resources-in-jar-properly
Made resources folder as Source and it worked. (right click on the folder "resources", go to "Build Path" > "Use as source folder)
