package com.geeksquest.ping.test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.geeksquest.ping.PingApplication;
import com.geeksquest.ping.util.Constants;

public class PingTest {
	private static int TEST_DELAY = 8000; // In Microseconds 
	@BeforeClass
	public static void setUp() {
		try {
			PingApplication.init();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Test
	public void testICMPPing() {
		String testHostName = "yahoo.com";
		PingApplication.sendIcmpPingRequest(testHostName);
		try {
			Thread.sleep(TEST_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String testString = "Reply from 98.139.180.180: bytes=1024";
		assertThat(PingApplication.getPingResponseMap().get(testHostName).get(Constants.ICMP_PING_RESPONSE_KEY),
				containsString(testString));
	}
	
	@Test
	public void testTCPPing() {
		String testHostName = "yahoo.com";
		PingApplication.sendTCPPingRequest(testHostName);
		try {
			Thread.sleep(TEST_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String testString = "HTTP/1.1 200 OKServer";
		assertThat(PingApplication.getPingResponseMap().get(testHostName).get(Constants.TCP_PING_RESPONSE_KEY),
				containsString(testString));
	}
	
	//@Test
	public void testTracePing() {
		String testHostName = "yahoo.com";
		PingApplication.sendTraceRouteRequest(testHostName);
		try {
			Thread.sleep(TEST_DELAY + TEST_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// TODO: Change testString and test it
		String testString = "Success";
		assertThat(PingApplication.getPingResponseMap().get(testHostName).get(Constants.TRACE_RESPONSE_KEY),
				containsString(testString));
	}
}
