package com.geeksquest.ping.command;

public interface Command {
	public void execute();
}
