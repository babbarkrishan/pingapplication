package com.geeksquest.ping.command;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.icmp4j.IcmpPingRequest;
import org.icmp4j.IcmpPingResponse;
import org.icmp4j.IcmpPingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geeksquest.ping.util.Constants;

public class ICMPPingCommand extends AbstractCommand {
	private static Logger LOGGER = LoggerFactory.getLogger(ICMPPingCommand.class);
	private IcmpPingRequest icmpPingRequest;	
	
	public ICMPPingCommand(String hostName) {
		this.hostName = hostName;
	}
	
	public ICMPPingCommand(String hostName, IcmpPingRequest icmpPingRequest, Map<String, ConcurrentHashMap<String, String>> pingResponseMap) {
		this.hostName = hostName;
		this.icmpPingRequest = icmpPingRequest;
		this.pingResponseMap = pingResponseMap;
	}
	
	@Override
	public void execute() {
		LOGGER.debug("execute Start: {}", hostName);
		try {
			/*LOGGER.debug("execute getHost: " + this.icmpPingRequest.getHost());
			LOGGER.debug("execute getPacketSize: " + this.icmpPingRequest.getPacketSize());
			LOGGER.debug("execute getTimeout: " + this.icmpPingRequest.getTimeout());
			LOGGER.debug("execute getTtl: " + this.icmpPingRequest.getTtl());*/
			
			IcmpPingResponse icmpPingResponse = IcmpPingUtil.executePingRequest(this.icmpPingRequest);
			final String formattedResponse = IcmpPingUtil.formatResponse (icmpPingResponse);
			
			this.updateLastResponse(Constants.ICMP_PING_RESPONSE_KEY, formattedResponse);
			
			if (!icmpPingResponse.getSuccessFlag()) {
				LOGGER.debug("Report Host: {}, Response: {}", hostName, formattedResponse);
				this.sendErrorReport(hostName);
			}
			else {
				LOGGER.debug("Last Successful Response Host: {}, Response: {}", hostName, formattedResponse);				
			}
			
		}
		catch (Exception ex) {
			LOGGER.error("Unable to connect host {}.", hostName, ex);
			this.updateLastResponse(Constants.ICMP_PING_RESPONSE_KEY, ex.getMessage());
			this.sendErrorReport(hostName);
		}
	}
	
	@Override
	public void run() {
		this.execute();
	}
}