package com.geeksquest.ping.command;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.icmp4j.IcmpTraceRouteRequest;
import org.icmp4j.IcmpTraceRouteResponse;
import org.icmp4j.IcmpTraceRouteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geeksquest.ping.util.Constants;

public class TraceRouteCommand extends AbstractCommand implements Command {
	private static Logger LOGGER = LoggerFactory.getLogger(TraceRouteCommand.class);
	private IcmpTraceRouteRequest icmpTraceRouteRequest;
	
	public TraceRouteCommand(String hostName) {
		this.hostName = hostName;
	}
	
	public TraceRouteCommand(String hostName, IcmpTraceRouteRequest icmpTraceRouteRequest, Map<String, ConcurrentHashMap<String, String>> pingResponseMap) {
		this.hostName = hostName;
		this.icmpTraceRouteRequest = icmpTraceRouteRequest;
		this.pingResponseMap = pingResponseMap;
	}
	
	
	@Override
	public void execute() {
		LOGGER.debug("execute Start: {}", hostName);
		try {
			/*LOGGER.debug("execute getHost: " + this.icmpTraceRouteRequest.getHost());
			LOGGER.debug("execute getPacketSize: " + this.icmpTraceRouteRequest.getPacketSize());
			LOGGER.debug("execute getTimeout: " + this.icmpTraceRouteRequest.getTimeout());
			LOGGER.debug("execute getTtl: " + this.icmpTraceRouteRequest.getTtl());*/
			
			IcmpTraceRouteResponse icmpTraceRouteResponse = IcmpTraceRouteUtil.executeTraceRoute(this.icmpTraceRouteRequest);
			
			this.updateLastResponse(Constants.TRACE_RESPONSE_KEY, icmpTraceRouteResponse.toString());			
			LOGGER.debug("Last Successful Response Host: {}, Response: {}", this.icmpTraceRouteRequest.getHost(), icmpTraceRouteResponse);			
		}
		catch (Exception ex) {
			LOGGER.error("Unable to connect host {}.", this.icmpTraceRouteRequest.getHost(), ex);
			this.updateLastResponse(Constants.TRACE_RESPONSE_KEY, ex.getMessage());
			this.sendErrorReport(hostName);
		}
	}
	
	@Override
	public void run() {
		this.execute();
	}

}
