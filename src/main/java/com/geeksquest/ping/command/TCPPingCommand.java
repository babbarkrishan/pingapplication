package com.geeksquest.ping.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geeksquest.ping.util.Constants;

public class TCPPingCommand extends AbstractCommand {
	private static Logger LOGGER = LoggerFactory.getLogger(TCPPingCommand.class);
	private final int TCP_REQUEST_PORT = 80;
	private final String TEST_STR = "Ping Request";
	
	public TCPPingCommand(String hostName, Map<String, ConcurrentHashMap<String, String>>pingResponseMap) {
		this.hostName = hostName;
		this.pingResponseMap = pingResponseMap;
	}

	@SuppressWarnings("resource")
	@Override
	public void execute() {
		LOGGER.debug("execute Start: {}", hostName);
		try {			
			Socket soc = new Socket(InetAddress.getByName(hostName), TCP_REQUEST_PORT);
			PrintWriter pw = new PrintWriter(soc.getOutputStream(), true);
			
			pw.println ("GET / HTTP/1.1");
			pw.println ("Host: " + hostName);
			pw.println("");
			//pw.flush();
			
			InputStreamReader inr = new InputStreamReader(soc.getInputStream());			
			BufferedReader br1 = new BufferedReader(inr);
			
			StringBuilder formattedResponse = new StringBuilder();
			
			String txt;
			while ((txt = br1.readLine()) != null) {
				formattedResponse.append(txt);
			}
			
			/*LOGGER.debug("Pinging {}, with string {}", soc.getInetAddress(), TEST_STR);
			LOGGER.debug("Reply from host: {}, Address: {}, String: {}, Length: {}", this.hostName, soc.getInetAddress(), formattedResponse, formattedResponse.length());
			LOGGER.debug("Sent: {}, Received: {}, Lost: {}", TEST_STR.length(), formattedResponse.length(), (TEST_STR.length() - formattedResponse.length()));*/
			
			LOGGER.debug("Last Successful Response Host: {}, Response: {}", hostName, formattedResponse);
			
			this.updateLastResponse(Constants.TCP_PING_RESPONSE_KEY, formattedResponse.toString());			
			
		} catch (IOException ex) {	
			LOGGER.error("Unable to connect host {}.", hostName, ex);
			this.updateLastResponse(Constants.TCP_PING_RESPONSE_KEY, ex.toString());
			this.sendErrorReport(hostName);
		}
	}
	
	@Override
	public void run() {
		this.execute();
	}
}
