package com.geeksquest.ping.command;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.geeksquest.ping.bean.Report;
import com.geeksquest.ping.service.ReportServiceImpl;
import com.geeksquest.ping.util.Constants;

public abstract class AbstractCommand implements Command, Runnable {
	
	protected String hostName;	
	protected Map<String, ConcurrentHashMap<String, String>> pingResponseMap;
	
	protected void updateLastResponse(String responseKey, String response) {
		ConcurrentHashMap<String, String> responseMap = this.pingResponseMap.get(hostName);
		if (responseMap == null) {
			responseMap = new ConcurrentHashMap<String, String>();
		}
		responseMap.put(responseKey, response);
		pingResponseMap.put(hostName, responseMap);
	}
	
	protected void sendErrorReport(String host) {
		Map<String, String> hostResponseMap = pingResponseMap.get(host);
		String icmePingLastRes = Constants.LAST_RESPONSE_NOT_AVAIALBLE;
		String tcpPingLastRes = Constants.LAST_RESPONSE_NOT_AVAIALBLE;
		String traceLastRes = Constants.LAST_RESPONSE_NOT_AVAIALBLE;
		
		if(hostResponseMap != null) {
			icmePingLastRes = hostResponseMap.get(Constants.ICMP_PING_RESPONSE_KEY) == null ? Constants.LAST_RESPONSE_NOT_AVAIALBLE : hostResponseMap.get(Constants.ICMP_PING_RESPONSE_KEY);
			tcpPingLastRes = hostResponseMap.get(Constants.TCP_PING_RESPONSE_KEY) == null ? Constants.LAST_RESPONSE_NOT_AVAIALBLE : hostResponseMap.get(Constants.TCP_PING_RESPONSE_KEY);
			traceLastRes = hostResponseMap.get(Constants.TRACE_RESPONSE_KEY) == null ? Constants.LAST_RESPONSE_NOT_AVAIALBLE : hostResponseMap.get(Constants.TRACE_RESPONSE_KEY);
		}
		Report errorReport = new Report();
		errorReport.setHost(host);
		errorReport.setIcmpPingResult(icmePingLastRes);
		errorReport.setTcpPingResult(tcpPingLastRes);
		errorReport.setTraceResult(traceLastRes);

		ReportServiceImpl.getInstance().reportAnIssue(errorReport);		
	}
}
