package com.geeksquest.ping;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.icmp4j.IcmpPingRequest;
import org.icmp4j.IcmpTraceRouteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geeksquest.ping.command.ICMPPingCommand;
import com.geeksquest.ping.command.TCPPingCommand;
import com.geeksquest.ping.command.TraceRouteCommand;
import com.geeksquest.ping.util.Constants;
import com.geeksquest.ping.util.PropertyLoader;

/**
 * @author Krishan Babbar
 * 
 * This Java application is using multi threading and can be used for checking health of any domain/server. 
 * This is a Maven based project. It checks health of any server through ICMP Ping, TCP and Trace Route on a configurable interval.
 * If server is running fine then it just adds logs otherwise add error in logs and send error report to an external server through POST API request.
 *
 */
public class PingApplication {
	private static Logger LOGGER = LoggerFactory.getLogger(PingApplication.class);

	private static ScheduledExecutorService scheduler;

	private static final int INITIAL_DEALY = 2; // In Seconds

	private static Map<String, ConcurrentHashMap<String, String>> pingResponseMap = null;

	public static void main(String[] args) {
		LOGGER.info("Starting Ping Application");
		try {
			init();
			String hosts = PropertyLoader.getProperty(Constants.HOSTS_KEY);

			String[] hostsArr = hosts.split(Constants.COMMA_DELIMITER);

			for (String hostName : hostsArr) {
				sendIcmpPingRequest(hostName);
				sendTCPPingRequest(hostName);
				sendTraceRouteRequest(hostName);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void init() throws IOException {
		PropertyLoader.load();
		int maxNoOfThreads = Integer.valueOf(PropertyLoader.getProperty(Constants.THREAD_POOL_THREADS_SIZE_KEY));
		scheduler = Executors.newScheduledThreadPool(maxNoOfThreads);
		pingResponseMap = new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();
	}

	public static void sendIcmpPingRequest(String hostName) {
		IcmpPingRequest icmpPingRequest = new IcmpPingRequest();
		icmpPingRequest.setHost(hostName);
		icmpPingRequest.setPacketSize(Integer.valueOf(PropertyLoader.getProperty(Constants.PING_PACKET_SIZE_KEY)));
		icmpPingRequest.setTimeout(Long.valueOf(PropertyLoader.getProperty(Constants.PING_TIMEOUT_KEY)));
		icmpPingRequest.setTtl(Integer.valueOf(PropertyLoader.getProperty(Constants.PING_TTL_KEY)));

		Long pingDelay = Long.valueOf(PropertyLoader.getProperty(Constants.PING_DELAY_KEY));

		Runnable icmpPingCmd = new ICMPPingCommand(hostName, icmpPingRequest, pingResponseMap);
		final ScheduledFuture<?> icmpPingHandle = scheduler.scheduleAtFixedRate(icmpPingCmd, INITIAL_DEALY, pingDelay,
				SECONDS);
	}

	public static void sendTCPPingRequest(String hostName) {
		Long pingDelay = Long.valueOf(PropertyLoader.getProperty(Constants.PING_DELAY_KEY));
		Runnable tcpPingCmd = new TCPPingCommand(hostName, pingResponseMap);
		final ScheduledFuture<?> tcpPingHandle = scheduler.scheduleAtFixedRate(tcpPingCmd, INITIAL_DEALY, pingDelay,
				SECONDS);
	}

	public static void sendTraceRouteRequest(String hostName) {
		IcmpTraceRouteRequest icmpTraceRouteRequest = new IcmpTraceRouteRequest();
		icmpTraceRouteRequest.setHost(hostName);
		icmpTraceRouteRequest
				.setPacketSize(Integer.valueOf(PropertyLoader.getProperty(Constants.PING_PACKET_SIZE_KEY)));
		icmpTraceRouteRequest.setTimeout(Long.valueOf(PropertyLoader.getProperty(Constants.PING_TIMEOUT_KEY)));
		icmpTraceRouteRequest.setTtl(Integer.valueOf(PropertyLoader.getProperty(Constants.PING_TTL_KEY)));

		Long tracertDelay = Long.valueOf(PropertyLoader.getProperty(Constants.TRACERT_DELAY_KEY));

		Runnable traceRouteCmd = new TraceRouteCommand(hostName, icmpTraceRouteRequest, pingResponseMap);
		final ScheduledFuture<?> traceRouteHandle = scheduler.scheduleAtFixedRate(traceRouteCmd, INITIAL_DEALY,
				tracertDelay, SECONDS);
	}

	public static Map<String, ConcurrentHashMap<String, String>> getPingResponseMap() {
		return pingResponseMap;
	}
}