package com.geeksquest.ping.bean;

public class Report {
	private String host;
	private String icmpPingResult;
	private String tcpPingResult;
	private String traceResult;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getIcmpPingResult() {
		return icmpPingResult;
	}

	public void setIcmpPingResult(String icmpPingResult) {
		this.icmpPingResult = icmpPingResult;
	}

	public String getTcpPingResult() {
		return tcpPingResult;
	}

	public void setTcpPingResult(String tcpPingResult) {
		this.tcpPingResult = tcpPingResult;
	}

	public String getTraceResult() {
		return traceResult;
	}

	public void setTraceResult(String traceResult) {
		this.traceResult = traceResult;
	}

	public String toString() {
		return "Report [host = " + this.host + ", icmpPingResult = " + this.icmpPingResult + ", tcpPingResult = "
				+ this.tcpPingResult + ", traceResult = " + this.traceResult + "]";
	}
}
