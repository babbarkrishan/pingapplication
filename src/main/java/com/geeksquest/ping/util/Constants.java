package com.geeksquest.ping.util;

public class Constants {
	public static final int RESPONSE_CODE_200 = 200;
	public static final int RESPONSE_CODE_201 = 201;
	public static final int RESPONSE_CODE_204 = 204;
	
	public static final String REPORT_API_URL_KEY = "report.post.api.url";
	
	public static final String HOSTS_KEY = "hosts";
	public static final String PING_DELAY_KEY = "ping.delay";	
	public static final String TRACERT_DELAY_KEY = "tracert.delay";
	public static final String THREAD_POOL_THREADS_SIZE_KEY = "thread.pool.threads.size";
	
	public static final String PING_TTL_KEY = "ping.ttl";
	public static final String PING_PACKET_SIZE_KEY = "ping.packet.size";
	public static final String PING_TIMEOUT_KEY = "ping.timeout";
			
	public static final String OS_NAME_KEY = "os.name";
	public static final String OS_NAME_WINDOWS = "Windows";
	
			
	public static final String COMMA_DELIMITER = ",";
	
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_ACCEPT = "Accept";
	public static final String HEADER_AUTHORIZATION = "Authorization";
	
	public static final String ACCEPT_TYPE_JSON = "application/json";	
	public static final String CONTENT_TYPE_JSON = "application/json";
	
	public static final String REPORT_HOST_KEY = "host";
	public static final String REPORT_ICMP_PING_KEY = "icmp_ping";
	public static final String REPORT_TCP_PING_KEY = "tcp_ping";
	public static final String REPORT_TRACE_PING_KEY = "trace";
	
	public static final String ICMP_PING_RESPONSE_KEY = "IcmpLastResponse";
	public static final String TCP_PING_RESPONSE_KEY = "TcpLastResponse";
	public static final String TRACE_RESPONSE_KEY = "TraceLastResponse";
	
	public static final String LAST_RESPONSE_NOT_AVAIALBLE = "NA";
	
}