package com.geeksquest.ping.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {
	private static final String PROPERTY_FILE = "config/ping.properties";
	private static Properties properties;
	
	
	public static void load() throws IOException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		properties = new Properties();
		try(InputStream resourceStream = loader.getResourceAsStream(PROPERTY_FILE)) {
			properties.load(resourceStream);
		}		
	}
	
	public static String getProperty(String propertyName) {
		return properties.getProperty(propertyName);
	}
}