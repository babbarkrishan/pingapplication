package com.geeksquest.ping.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geeksquest.ping.bean.Report;
import com.geeksquest.ping.util.Constants;
import com.geeksquest.ping.util.PropertyLoader;

public class ReportServiceImpl implements ReportService {
	private static Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);
	private static ReportServiceImpl instance = null;
	
	private ReportServiceImpl() {		
	}
	
	public static ReportServiceImpl getInstance() {		
		if (instance == null) {
            synchronized (ReportServiceImpl.class) {
                if (instance == null) {
                	instance = new ReportServiceImpl();
                }
            }
        }
        return instance;	
	}
	
	@Override
	public void reportAnIssue(Report report) {
		LOGGER.error("reportAnIssue Report: {}", report.toString());
		this.sendReportToServer(report);		
	}	
	
	public void sendReportToServer(Report report) {
		LOGGER.debug("sendReportToServer Report: {}", report.toString());
		try {
			String sendReportAPIURL = PropertyLoader.getProperty(Constants.REPORT_API_URL_KEY);

			HttpPost reportHttpPost = new HttpPost(sendReportAPIURL);

			// add header
			reportHttpPost.setHeader(Constants.HEADER_CONTENT_TYPE, Constants.CONTENT_TYPE_JSON);
			reportHttpPost.setHeader(Constants.HEADER_ACCEPT, Constants.ACCEPT_TYPE_JSON);			
			
			StringEntity entity = new StringEntity(this.getJsonBody(report));

			reportHttpPost.setEntity(entity);

			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse reportResponse = client.execute(reportHttpPost);

			LOGGER.debug("sendReportToServer - Response Code : " + reportResponse.getStatusLine().getStatusCode());

			if (reportResponse.getStatusLine().getStatusCode() != Constants.RESPONSE_CODE_200) {
				throw new RuntimeException("sendReportToServer - failed to submit report: " + reportResponse.getStatusLine().getStatusCode());
			}
			else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(reportResponse.getEntity().getContent()));
				String line = "";
				StringBuffer result = new StringBuffer();
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
				LOGGER.debug("sendReportToServer - Response: " + result.toString());
			}
			
			// close the client.
			((CloseableHttpClient) client).close();
		} catch (Exception ex) {
			LOGGER.error("sendReportToServer Report - Unable to submit report.", ex);
		}
	}

	@SuppressWarnings("unchecked")
	private String getJsonBody(Report report) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(Constants.REPORT_HOST_KEY, report.getHost());
		jsonObject.put(Constants.REPORT_ICMP_PING_KEY, report.getIcmpPingResult());
		jsonObject.put(Constants.REPORT_TCP_PING_KEY, report.getTcpPingResult());
		jsonObject.put(Constants.REPORT_TRACE_PING_KEY, report.getTraceResult());		
		return jsonObject.toString();
	}

	
}
