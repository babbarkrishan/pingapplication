package com.geeksquest.ping.service;

import com.geeksquest.ping.bean.Report;

public interface ReportService {
	public void reportAnIssue(Report report);
}
